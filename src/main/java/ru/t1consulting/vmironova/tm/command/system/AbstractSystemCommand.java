package ru.t1consulting.vmironova.tm.command.system;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1consulting.vmironova.tm.api.service.ICommandService;
import ru.t1consulting.vmironova.tm.command.AbstractCommand;
import ru.t1consulting.vmironova.tm.enumerated.Role;

public abstract class AbstractSystemCommand extends AbstractCommand {

    @NotNull
    protected ICommandService getCommandService() {
        assert serviceLocator != null;
        return serviceLocator.getCommandService();
    }

    @Nullable
    public Role[] getRoles() {
        return null;
    }

}
