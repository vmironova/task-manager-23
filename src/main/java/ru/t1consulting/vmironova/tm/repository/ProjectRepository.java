package ru.t1consulting.vmironova.tm.repository;

import org.jetbrains.annotations.NotNull;
import ru.t1consulting.vmironova.tm.api.repository.IProjectRepository;
import ru.t1consulting.vmironova.tm.model.Project;

import java.util.Objects;

public final class ProjectRepository extends AbstractUserOwnedRepository<Project> implements IProjectRepository {

    @NotNull
    @Override
    public Project create(@NotNull final String userId,
                          @NotNull final String name,
                          @NotNull final String description
    ) {
        @NotNull final Project project = new Project(name, description);
        project.setUserId(userId);
        return Objects.requireNonNull(add(project));
    }

    @NotNull
    @Override
    public Project create(@NotNull final String userId,
                          @NotNull final String name
    ) {
        @NotNull final Project project = new Project(name);
        project.setUserId(userId);
        return Objects.requireNonNull(add(project));
    }

}
